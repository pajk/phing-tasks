<?php
require_once __DIR__ . '/../../../../autoload.php';

class ExtractLastMigrationTask extends Task
{
    protected $from = "PHINX_STATUS";
    protected $target = "PHINX_CURRENT";
    protected $failonerror = false;

    /**
     * File to read
     *
     * @param  string $from
     * @return void
     */
    public function setFrom($input)
    {
        if (!is_file($input)) {
            throw new BuildException(sprintf(
                'File does not exist: %s',
                $input
            ));
        }
        $this->from = realpath($input);
    }

    public function setTarget($target)
    {
        $this->target = $target;
    }

    /**
     * if error occured, whether build should fail
     *
     * @param  bool $value
     * @return void
     */
    public function setFailonerror($value)
    {
        $this->failonerror = $value;
    }

    /**
     * init
     *
     * @return void
     */
    public function init()
    {
    }

    /**
     * main method
     *
     * @return void
     */
    public function main()
    {
        if (!$this->from) {
            throw new BuildException('status file must be provided');
        }

        $content = file_get_contents($this->from);

        preg_match_all('/^     up  (\d+)  \w+/m', $content, $m);

        if (count($m) > 0 && count($m[1]) > 0) {
            $current = $m[1][count($m[1])-1];
        } else {
            $current = 0;
        }

        file_put_contents($this->target, $current);

        $this->log(sprintf(
            'Current migration is \'%s\'',
            $current
        ));
    }
}